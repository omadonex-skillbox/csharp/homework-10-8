﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_10_8
{
    internal interface IClientEditable
    {
        public void editClient(int id, Dictionary<string, string> newData);
    }
}

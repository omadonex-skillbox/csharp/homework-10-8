﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_10_8
{
    internal class Consultant : IClientEditable
    {
        protected Repository repository;

        public Consultant(Repository repository) 
        {
            this.repository = repository;
        }

        public virtual int[] GetAvailableOptions()
        {
            return new int[] { 0, 1, 3 };
        }

        public virtual string[] GetAvailableFillingFields()
        {
            return Client.FillableFields.Intersect(new string[] { "phoneNumber" }).ToArray();            
        }

        public void ViewClientById(int id)
        {
            PrintClient(repository.GetClientById(id));
        }

        public void ViewAllClients()
        {
            foreach(Client client in repository.GetAllClients())
            {
                PrintClient(client);
            }
        }

        public int CountClients()
        {
            return repository.Count();
        }

        protected virtual void PrintClient(Client client)
        {
            Dictionary<string, string> clientData = client.PrintableData();
            clientData["passportSeries"] = "HIDDEN";
            clientData["passportNumber"] = "HIDDEN";
            foreach(KeyValuePair<string, string> pair in clientData)
            {
                Console.WriteLine($"{pair.Key}: {pair.Value}");                                
            }
            Console.WriteLine("");
        }

        public void editClient(int id, Dictionary<string, string> newData)
        {
            repository.EditClientById(this, id, newData);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_10_8
{
    internal class Manager : Consultant, IClientCreatable
    {
        public Manager(Repository repository) : base(repository) { }

        public override int[] GetAvailableOptions()
        {
            return new int[] { 0, 1, 2, 3 };
        }

        public override string[] GetAvailableFillingFields()
        {
            return Client.FillableFields.Intersect(
                new string[] { "firstName", "lastName", "optName", "passportSeries", "passportNumber", "phoneNumber" }
                ).ToArray();            
        }

        public void CreateClient(Dictionary<string, string> data)
        {
            repository.AddClient(this, data);
        }

        protected override void PrintClient(Client client)
        {
            Dictionary<string, string> clientData = client.PrintableData();
            foreach (KeyValuePair<string, string> pair in clientData)
            {
                Console.WriteLine($"{pair.Key}: {pair.Value}");
            }
            client.PrintMetaData();
            Console.WriteLine("");
        }
    }
}
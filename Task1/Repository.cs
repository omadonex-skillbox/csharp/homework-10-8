﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork_10_8
{
    /// <summary>
    /// В рамках этого задания хотелось поиграться с JSON
    /// Сохранение данных в файл организовано не целиком для List, а построчно для каждого Client (но строка представляет собой JSON)
    /// Выбор такой структуры обоснован оптимизацией добавления новых Client 
    /// (если бы мы сохраняли List целиком, то пришлось бы перезаписывать весь файл, а так достаточно добавить только одну строку в конец)
    /// (разумеется в случае с удалением и редактированием будем перезаписывать весь файл)
    /// </summary>
    internal class Repository
    {
        private int maxId;
        private string path;
        private List<Client> data;

        public Repository(string path)
        {
            data = new List<Client>();
            maxId = 0;
            this.path = path;
            Load();         
        }

        private int NextId()
        {
            return ++maxId;
        }

        private void Load()
        {
            if (File.Exists(path))
            {
                string[] lines = File.ReadAllLines(path);
                foreach (string line in lines)
                {
                    Client client = JsonConvert.DeserializeObject<Client>(line);
                    if (maxId < client.Id)
                    {
                        maxId = client.Id;
                    }

                    data.Add(client);                    
                }
                
            }
            else {
                Console.WriteLine("Database not exists!");
                File.Create(path).Close();
                Console.WriteLine("New database was created.");                
            }
        }

        public int Count()
        {
            return data.Count;
        }

        public List<Client> GetAllClients()
        {
            return data;
        }               

        public void AddClient(Consultant user, Dictionary<string, string> fillableData)
        {            
            Client client = new Client(NextId(), fillableData);
            client.SetMetaData(user.GetType().Name, "created");
            data.Add(client);
            
            string[] lines = new string[1];
            lines[0] = JsonConvert.SerializeObject(client);
            File.AppendAllLines(path, lines);
        }

        public void EditClientById(Consultant user, int id, Dictionary<string, string> newData)
        {
            Client client = GetClientById(id);
            client.updateData(newData);
            client.SetMetaData(user.GetType().Name, "updated");
            saveData();
        }

        public Client GetClientById(int id)
        {
            Client client = data.Find(client => client.Id == id);

            if (client == null)
            {
                throw new Exception("Client not found");
            }           

            return client;
        }

        private void saveData()
        {
            string[] lines = new string[data.Count];
            int index = 0;
            foreach (Client client in data)
            {
                lines[index] = JsonConvert.SerializeObject(client);
                index++;
            }

            File.WriteAllLines(path, lines);
        }
    }
}

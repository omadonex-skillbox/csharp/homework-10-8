﻿namespace HomeWork_10_8
{
    internal class Program
    {
        private const string DataPath = "data.txt";
        private static Dictionary<int, string> optionList = new Dictionary<int, string>()
            {
                { 1, "Get info about clients" },
                { 2, "Add new client" },
                { 3, "Edit client" },
            };

        private static Consultant user;

        private static void Main(string[] args)
        {            
            Console.WriteLine("Hello, input work mode 1 - consultant, 2 - manager!");            
            user = int.Parse(Console.ReadLine()) == 1 ? 
                new Consultant(new Repository(DataPath)) : 
                new Manager(new Repository(DataPath));

            int option;
            do
            {
                
                Console.WriteLine("");
                Console.WriteLine("==================================");
                Console.WriteLine("Choose an option:");
                foreach (KeyValuePair<int, string> kvp in optionList)
                {
                    if (user.GetAvailableOptions().Contains(kvp.Key))
                    {
                        Console.WriteLine($"{kvp.Key} - {kvp.Value}");
                    }
                }                
                Console.WriteLine("0 - Exit");
                Console.WriteLine("==================================");
                Console.WriteLine("");

                if (!int.TryParse(Console.ReadLine(), out option) || !user.GetAvailableOptions().Contains(option))
                {
                    string optionsStr = string.Join(',', user.GetAvailableOptions());
                    Console.WriteLine($"Incorrect option. Available options: [{optionsStr}]");
                }
                else
                {
                    switch (option)
                    {
                        case 1:
                            readAllMode();
                            break;
                        case 2:
                            addNewMode();
                            break;
                        case 3:
                            editMode();
                            break;
                    }
                }
            } while (option != 0);
        }

        private static void readAllMode()
        {
            Console.WriteLine("");
            Console.WriteLine("##### Getting clients data #####");

            user.ViewAllClients();
            Console.WriteLine($"Total records count: {user.CountClients()}");
        }

        private static void addNewMode()
        {
            Console.WriteLine("");
            Console.WriteLine("##### Input new client data #####");
            
            Dictionary<string, string> data = new Dictionary<string, string>();
            foreach (string field in user.GetAvailableFillingFields())
            {
                Console.WriteLine($"input '{field}'");
                data.Add(field, Console.ReadLine());
            }

            (user as Manager).CreateClient(data);
            Console.WriteLine("New Client successfully added");
        }

        private static void editMode()
        {
            Console.WriteLine("");
            Console.WriteLine("##### Editing Client #####");
            Console.WriteLine("Input client id:");
            int id = int.Parse(Console.ReadLine());
            try
            {
                Console.WriteLine("Search result:");
                user.ViewClientById(id);                

                Console.WriteLine("");
                string fillingFieldsStr = string.Join(", ", user.GetAvailableFillingFields());

                Console.WriteLine($"You can edit only [{fillingFieldsStr}]");
                Dictionary<string, string> newData = new Dictionary<string, string>();
                foreach (string field in user.GetAvailableFillingFields())
                {
                    Console.WriteLine($"Input new '{field}' or leave blank to not made changes");
                    newData.Add(field, Console.ReadLine());
                }

                user.editClient(id, newData);

                Console.WriteLine("Client successfully edited");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
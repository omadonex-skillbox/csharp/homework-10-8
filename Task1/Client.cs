﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HomeWork_10_8
{
    internal class Client
    {
        [JsonProperty("id")]
        private int id;       
        [JsonProperty("firstName")]
        private string firstName;
        [JsonProperty("lastName")]
        private string lastName;
        [JsonProperty("optName")]
        private string optName;
        [JsonProperty("passportSeries")]
        private string passportSeries;
        [JsonProperty("passportNumber")]
        private string passportNumber;
        [JsonProperty("phoneNumber")]
        private string phoneNumber;
        [JsonProperty("modifiedAt")]
        private DateTime modifiedAt;
        [JsonProperty("modifiedUserType")]
        private string modifiedUserType;
        [JsonProperty("modifiedType")]
        private string modifiedType;

        public static string[] FillableFields = { "firstName", "lastName", "optName", "passportSeries", "passportNumber", "phoneNumber" };

        [JsonIgnore]
        public int Id { get { return id; } }

        public Client()
        {
            id = 0;
        }

        public void updateData(Dictionary<string, string> newData)
        {
            //TODO: Здесь наверное можно динамически изменять значения свойств по имени, но мы пока что это не проходили ))
            foreach(KeyValuePair<string, string> pair in newData)
            {
                if (pair.Value != null && pair.Value != "")
                {
                    FieldInfo fi = typeof(Client).GetField(pair.Key, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    fi.SetValue(this, pair.Value);
                }
            }
        }

        public Client(int id, Dictionary<string, string> fillableData) 
        {
            this.id = id;
            foreach (string field in FillableFields)
            {
                if (fillableData[field] != null)
                {
                    FieldInfo fi = typeof(Client).GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    fi.SetValue(this, fillableData[field]);                    
                }                
            }
        }

        public Dictionary<string, string> PrintableData()
        {
            Dictionary<string, string> data = new Dictionary<string, string>()
            {
                { "id", id.ToString() },
            };

            foreach (string field in FillableFields)
            {
                FieldInfo fi = typeof(Client).GetField(field, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);                                
                data.Add(field, fi.GetValue(this).ToString());                
            }

            return data;
        }    

        public void PrintMetaData()
        {
            Console.WriteLine($"{modifiedType} by {modifiedUserType} at {modifiedAt}");
        }

        public void SetMetaData(string modifiedUserType, string modifiedType)
        {
            modifiedAt = DateTime.Now;
            this.modifiedUserType = modifiedUserType;
            this.modifiedType = modifiedType;
        }
    }
}
